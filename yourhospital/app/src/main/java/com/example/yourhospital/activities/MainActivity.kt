package com.teme.healthsoft.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.teme.healthsoft.fragments.FragmentSlider
import com.teme.healthsoft.adapters.PagerAdapter
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var preference : SharedPreferences
    val pref_show_intro = "Intro"

    val fragment1 = FragmentSlider()
    val fragment2 = FragmentSlider()
    val fragment3 = FragmentSlider()

    lateinit var adapter : PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preference = getSharedPreferences("IntroSlider", Context.MODE_PRIVATE)
        if(!preference.getBoolean(pref_show_intro, true)){
            startActivity(Intent(this, SignInSignUpActivity::class.java))
            finish()
        }

        fragment1.setTitle("Easy to make an appointment")
        fragment1.setImage(R.drawable.appointment)
        fragment2.setTitle("Skilled doctors")
        fragment2.setImage(R.drawable.doctors)
        fragment3.setTitle("Free medical examination")
        fragment3.setImage(R.drawable.medicalexamination)

        adapter =
            PagerAdapter(supportFragmentManager)
        adapter.list.add(fragment1)
        adapter.list.add(fragment2)
        adapter.list.add(fragment3)

        view_pager.adapter = adapter

        btn_next.setOnClickListener {
            view_pager.currentItem++
        }

        btn_skip.setOnClickListener {
            startActivity(Intent(this@MainActivity, SignInSignUpActivity::class.java))
            finish()
            val editor = preference.edit()
            editor.putBoolean(pref_show_intro, false)
            editor.apply()
        }

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if(position == adapter.list.size - 1){
                    btn_next.text = "Done"
                    btn_next.setOnClickListener {
                        startActivity(Intent(this@MainActivity, SignInSignUpActivity::class.java))
                        finish()
                        val editor = preference.edit()
                        editor.putBoolean(pref_show_intro, false)
                        editor.apply()
                    }
                    btn_skip.setOnClickListener {
                        startActivity(Intent(this@MainActivity, SignInSignUpActivity::class.java))
                        finish()
                        val editor = preference.edit()
                        editor.putBoolean(pref_show_intro, false)
                        editor.apply()
                    }
                }else{
                    btn_next.text = "Next"
                    btn_next.setOnClickListener{
                        view_pager.currentItem++
                    }
                    btn_skip.setOnClickListener {
                        startActivity(Intent(this@MainActivity, SignInSignUpActivity::class.java))
                        finish()
                        val editor = preference.edit()
                        editor.putBoolean(pref_show_intro, false)
                        editor.apply()
                    }
                }
            }

        })
    }
}
