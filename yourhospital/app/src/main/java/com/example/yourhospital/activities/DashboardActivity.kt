package com.teme.healthsoft.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.teme.healthsoft.R
import com.teme.healthsoft.fragments.*
import kotlinx.android.synthetic.main.nav_header.*
import kotlinx.android.synthetic.main.nav_header.view.*
import java.io.ByteArrayOutputStream
import java.io.File


class DashboardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    private lateinit var imageUri: Uri
    private val REQUEST_IMAGE_CAPTURE = 100
    val storageRef = FirebaseStorage
        .getInstance()
        .reference.child("pics/${FirebaseAuth.getInstance().currentUser?.uid}")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fl_container, FragmentHome(), "FragmentHome")
        transaction.commit()

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
        var firebaseAuth = FirebaseAuth.getInstance()
        var user = firebaseAuth.currentUser
        if (user != null) {
            val headerView = navView.getHeaderView(0)
            val email = headerView.findViewById<TextView>(R.id.tv_email)
            email.text = user?.email
        }
        val headerView = navView.getHeaderView(0)
        val img = findViewById<ImageView>(R.id.image_view_profile)
        headerView.image_view_profile.setOnClickListener {
            takePictureIntent()
        }

        var file: File = createTempFile("image", "jpeg")

        storageRef.getFile(file).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(file.absolutePath)
            headerView.image_view_profile.setImageBitmap(bitmap)
        }
    }

    private fun takePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { pictureIntent ->
            pictureIntent.resolveActivity(this.packageManager!!).also {
                startActivityForResult(pictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap

            uploadImage(imageBitmap)
        }
    }

    private fun uploadImage(bitmap: Bitmap) {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val image = baos.toByteArray()
        val upload = storageRef.putBytes(image)

        upload.addOnCompleteListener { uploadTask ->
            if (uploadTask.isSuccessful) {
                storageRef.downloadUrl.addOnCompleteListener { urlTask ->
                    urlTask.result?.let {
                        imageUri = it
                        Toast.makeText(this, imageUri.toString(), Toast.LENGTH_SHORT).show()

                        image_view_profile.setImageBitmap(bitmap)
                    }
                }
            } else {
                uploadTask.exception?.let {
                    Toast.makeText(this, it.message!!, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val transaction = supportFragmentManager.beginTransaction()
        when (item.itemId) {
            R.id.nav_home -> {
                transaction.replace(R.id.fl_container, FragmentHome(), "FragmentHome")
                transaction.addToBackStack("FragmentHome")
                transaction.commit()
            }
            R.id.nav_doctors -> {
                transaction.replace(R.id.fl_container, FragmentDoctors(), "FragmentDoctors")
                transaction.addToBackStack("FragmentDoctors")
                transaction.commit()
            }
            R.id.nav_appointments -> {
                transaction.replace(
                    R.id.fl_container,
                    FragmentAppointments(),
                    "FragmentAppointments"
                )
                transaction.addToBackStack("FragmentAppointments")
                transaction.commit()
            }
            R.id.nav_update_profile -> {
                transaction.replace(
                    R.id.fl_container,
                    FragmentUpdateProfile(),
                    "FragmentUpdateProfile"
                )
                transaction.addToBackStack("FragmentUpdateProfile")
                transaction.commit()
            }
            R.id.nav_logout -> {
                AlertDialog.Builder(this).apply {
                    setTitle("Are you sure?")
                    setPositiveButton("Yes") { _, _ ->
                        FirebaseAuth.getInstance().signOut()
                        startActivity(
                            Intent(
                                this@DashboardActivity,
                                SignInSignUpActivity::class.java
                            )
                        )
                        finish()
                    }
                    setNegativeButton("Cancel") { _, _ ->

                    }
                }.create().show()
            }
            R.id.nav_contact -> {
                transaction.replace(R.id.fl_container, FragmentContact(), "FragmentContact")
                transaction.addToBackStack("FragmentContact")
                transaction.commit()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
